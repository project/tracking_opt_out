<?php

namespace Drupal\trackingoptout\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'TrackingOptOut' Block.
 *
 * @Block(
 *   id = "trackingoptout_block",
 *   admin_label = @Translation("Tracking opt-out block"),
 *   category = @Translation("trackingoptout"),
 * )
 */
class TrackingOptOutBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()
      ->getForm('Drupal\trackingoptout\Form\TrackingOptOutForm');
  }

}
