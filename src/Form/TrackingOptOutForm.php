<?php

namespace Drupal\trackingoptout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class TrackingOptOutForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected $providersManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->providersManager = $container->get('trackingoptout.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_trackingoptout_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'trackingoptout/trackingoptout-js';
    $form['#attached']['library'][] = 'trackingoptout/trackingoptout-base';
    $form['#attached']['drupalSettings']['defaultTracking'] = $this->providersManager->getDefaultTrackingConfig();

    if (!empty($this->providersManager->getDisablingProviders())) {
      $form['optout_checkbox'] = [
        '#type' => 'checkbox',
        '#attributes' => ['class' => ['tracking-optout__input']],
        '#theme_wrappers' => ['trackingoptout_wrapper'],
      ];
      return $form;
    }

    $form["#markup"] = $this->t("You don't have configured any provider");
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
