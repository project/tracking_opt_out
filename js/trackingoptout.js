(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.trackingOptOut = {
    attach: function () {
      $('body').once('trackingoptout').each(function () {
        if (trackingOptout.enabled) {
          trackingOptOutLoadScripts();
        }
      });
    },
    defaultTracking: function () {
      return drupalSettings.defaultTracking;
    },
    // API methods
    doNotTrack: function () {
      return trackingOptout.doNotTrack;
    },
    enabled: function () {
      return trackingOptout.enabled;
    },
    toggle: function () {
      return trackingOptout.toggle();
    },
    set: function (value) {
      return trackingOptout.set(value);
    },
  };
}(jQuery, Drupal, drupalSettings));
